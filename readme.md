# PolyWrench
PolyWrench is a minecraft plugin that adds a wrench that can rotate and tweak blocks:

- Change axis of logs
- Rotate blocks like furnaces, pistons, beehives
- Rotate mob/player heads, floor signs, floor banners
- Tweak fences, panes, iron bars, walls
- Rotate/tweak rails
- Make non-empty item frames invisible