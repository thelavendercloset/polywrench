package fr.aquaxoc.polywrench;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;


public class CommandWrench implements CommandExecutor {

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, String[] args) {
        if (args.length == 0) {
            if (sender instanceof Player) {
                Player player = (Player) sender;
                player.getInventory().addItem(PolyWrench.Wrench);
            }
        } else {
            for (String playername : args) {
                Player player = Bukkit.getPlayer(playername);
                if (player != null) {
                    player.getInventory().addItem(PolyWrench.Wrench);
                } else {
                    sender.sendMessage("Player "+playername+" not found.");
                }
            }
        }
        return true;
    }
}
