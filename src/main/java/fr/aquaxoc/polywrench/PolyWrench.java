package fr.aquaxoc.polywrench;

import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.*;
import org.bukkit.block.data.type.*;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDropItemEvent;
import org.bukkit.event.hanging.HangingBreakEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.RayTraceResult;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public final class PolyWrench extends JavaPlugin implements Listener {

    private static final BlockFace[] ROTATE_CCW = new BlockFace[] {
            BlockFace.EAST,
            BlockFace.EAST_NORTH_EAST,
            BlockFace.NORTH_EAST,
            BlockFace.NORTH_NORTH_EAST,
            BlockFace.NORTH,
            BlockFace.NORTH_NORTH_WEST,
            BlockFace.NORTH_WEST,
            BlockFace.WEST_NORTH_WEST,
            BlockFace.WEST,
            BlockFace.WEST_SOUTH_WEST,
            BlockFace.SOUTH_WEST,
            BlockFace.SOUTH_SOUTH_WEST,
            BlockFace.SOUTH,
            BlockFace.SOUTH_SOUTH_EAST,
            BlockFace.SOUTH_EAST,
            BlockFace.EAST_SOUTH_EAST
    };
    private static final EnumMap<BlockFace, BlockFace> ROTATE_CCW_MAP =
            new EnumMap<>(BlockFace.class);
    static {
        for (int i = 0; i < ROTATE_CCW.length; ++i) {
            ROTATE_CCW_MAP.put(ROTATE_CCW[i], ROTATE_CCW[(i + 1) % ROTATE_CCW.length]);
        }
    }

    private static final Set<Material> BANNED_DIRECTIONAL = new HashSet<>();
    static {
        for (final Material mat : Material.values()) {
            if (mat.name().endsWith("_WALL_BANNER")) {
                BANNED_DIRECTIONAL.add(mat);
            }
        }
        BANNED_DIRECTIONAL.add(Material.WALL_TORCH);
        BANNED_DIRECTIONAL.add(Material.SOUL_WALL_TORCH);
    }

    public NamespacedKey WrenchKey = new NamespacedKey(this, "wrench");

    static public ItemStack Wrench = new ItemStack(Material.WOODEN_HOE);

    private ConcurrentHashMap<Block, LastRailState> lastRailFace = new ConcurrentHashMap<>();

    @Override
    public void onEnable() {
        // Plugin startup logic
        getServer().getPluginManager().registerEvents(this, this);

        ItemMeta wrenchMeta = Wrench.getItemMeta();
        assert wrenchMeta != null;
        wrenchMeta.setDisplayName("\u00A7rWrench");
        wrenchMeta.getPersistentDataContainer().set(WrenchKey, PersistentDataType.BYTE, (byte) 1);
        wrenchMeta.setCustomModelData("fr.aquaxoc.polywrench".hashCode() & 0x7FFFFF);

        Wrench.setItemMeta(wrenchMeta);

        Objects.requireNonNull(this.getCommand("wrench")).setExecutor(new CommandWrench());

        getServer().getScheduler().runTaskTimer(this, this::clearLastRailFace, 2400, 2400);
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }


    private void clearLastRailFace() {
        synchronized (lastRailFace) {
            for (Map.Entry<Block, LastRailState> entry : lastRailFace.entrySet()) {
                if (entry.getValue().dateUsed + 120000 < System.currentTimeMillis()) {
                    lastRailFace.remove(entry.getKey());
                }
            }
        }
    }


    @EventHandler(ignoreCancelled = true)
    public void onWrenchInteract(PlayerInteractEvent event) {
        if (!event.hasBlock() || !event.hasItem()) return;
        if (!event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) return;
        //if (!Objects.requireNonNull(event.getItem()).isSimilar(Wrench)) return;
        final @Nullable ItemMeta meta = Objects.requireNonNull(event.getItem()).getItemMeta();
        if (meta == null) return;
        if (!Objects.equals(meta.getPersistentDataContainer().get(WrenchKey, PersistentDataType.BYTE), (byte) 1)) return;
        event.setCancelled(true);

        final Player player = event.getPlayer();
        if (!player.hasPermission("polywrench.use")) return;

        final Block block = Objects.requireNonNull(event.getClickedBlock());
        final BlockData blockData = block.getBlockData();

        if (isDirectional(block)) {
            if (!canBuild(event, block, player, blockData)) return;
            BlockFace rotation = ((Directional) block.getBlockData()).getFacing();

            Set<BlockFace> blockFaces = ((Directional) block.getBlockData()).getFaces();

            // Handling double chests
            if (blockData instanceof Chest && !((Chest) blockData).getType().equals(Chest.Type.SINGLE)) {
                Chest.Type chestType = ((Chest) blockData).getType();
                boolean isLeft = chestType.equals(Chest.Type.LEFT);
                if (isLeft) ((Chest) blockData).setType(Chest.Type.RIGHT);
                else ((Chest) blockData).setType(Chest.Type.LEFT);

                Block otherBlock;
                if (rotation == BlockFace.EAST) {
                    ((Chest) blockData).setFacing(BlockFace.WEST);
                    otherBlock = block.getRelative(isLeft ? BlockFace.SOUTH : BlockFace.NORTH);
                } else if (rotation == BlockFace.WEST) {
                    ((Chest) blockData).setFacing(BlockFace.EAST);
                    otherBlock = block.getRelative(isLeft ? BlockFace.NORTH : BlockFace.SOUTH);
                } else if (rotation == BlockFace.NORTH) {
                    ((Chest) blockData).setFacing(BlockFace.SOUTH);
                    otherBlock = block.getRelative(isLeft ? BlockFace.EAST: BlockFace.WEST);
                } else {
                    ((Chest) blockData).setFacing(BlockFace.NORTH);
                    otherBlock = block.getRelative(isLeft ? BlockFace.WEST : BlockFace.EAST);
                }
                Chest otherBlockData = (Chest) otherBlock.getBlockData();
                otherBlockData.setFacing(((Chest) blockData).getFacing());
                otherBlockData.setType(isLeft ? Chest.Type.LEFT : Chest.Type.RIGHT);
                otherBlock.setBlockData(otherBlockData, false);
            } else {
                // NORTH → (UP →) EAST → SOUTH → (DOWN →) WEST

                if (blockFaces.contains(BlockFace.UP) && rotation == BlockFace.NORTH) {
                    ((Directional) blockData).setFacing(BlockFace.UP);
                } else if (rotation == BlockFace.NORTH) {
                    ((Directional) blockData).setFacing(BlockFace.EAST);
                } else if (rotation == BlockFace.UP) {
                    ((Directional) blockData).setFacing(BlockFace.EAST);
                } else if (rotation == BlockFace.EAST) {
                    ((Directional) blockData).setFacing(BlockFace.SOUTH);
                } else if (blockFaces.contains(BlockFace.DOWN) && rotation == BlockFace.SOUTH) {
                    ((Directional) blockData).setFacing(BlockFace.DOWN);
                } else if (rotation == BlockFace.SOUTH) {
                    ((Directional) blockData).setFacing(BlockFace.WEST);
                } else if (rotation == BlockFace.DOWN) {
                    ((Directional) blockData).setFacing(BlockFace.WEST);
                } else if (rotation == BlockFace.WEST) {
                    ((Directional) blockData).setFacing(BlockFace.NORTH);
                }
            }
            block.setBlockData(blockData, !(block instanceof Door));
        } else if (isMultipleFacing(block)) {
            if (!canBuild(event, block, player, blockData)) return;
            final BlockFace face = getPointedToDirection(event.getPlayer());
            final MultipleFacing facingData = (MultipleFacing) blockData;
            facingData.setFace(face, !facingData.hasFace(face));
            block.setBlockData(facingData, false);
        } else if (blockData instanceof Wall) {
            if (!canBuild(event, block, player, blockData)) return;
            final Wall wallData = (Wall) blockData;
            if (event.getPlayer().isSneaking()) {
                // Toggle "up" value
                final Wall.Height hNorth = wallData.getHeight(BlockFace.NORTH);
                final Wall.Height hEast = wallData.getHeight(BlockFace.EAST);
                final Wall.Height hSouth = wallData.getHeight(BlockFace.SOUTH);
                final Wall.Height hWest = wallData.getHeight(BlockFace.WEST);
                if (!(hNorth==hSouth && hEast==hWest && (hNorth != Wall.Height.NONE || hEast != Wall.Height.NONE))) return;
                ((Wall) blockData).setUp(!((Wall) blockData).isUp());
            } else {
                final BlockFace face = getPointedToDirection(event.getPlayer());
                final Wall.Height height = wallData.getHeight(face);
                switch (height) {
                    case NONE: wallData.setHeight(face, Wall.Height.LOW); break;
                    case LOW: wallData.setHeight(face, Wall.Height.TALL); break;
                    case TALL: wallData.setHeight(face, Wall.Height.NONE); break;
                }
                final Wall.Height hNorth = wallData.getHeight(BlockFace.NORTH);
                final Wall.Height hEast = wallData.getHeight(BlockFace.EAST);
                final Wall.Height hSouth = wallData.getHeight(BlockFace.SOUTH);
                final Wall.Height hWest = wallData.getHeight(BlockFace.WEST);
                wallData.setUp(hNorth != hSouth || hEast != hWest ||
                        (hNorth == Wall.Height.NONE && hEast == Wall.Height.NONE &&
                        hSouth == Wall.Height.NONE && hWest == Wall.Height.NONE));
                if (!wallData.isUp() && hNorth != Wall.Height.NONE && hEast != Wall.Height.NONE && hNorth != hEast) {
                    wallData.setUp(true);
                }
            }
            block.setBlockData(wallData, false);
        } else if (isOrientable(block)) {
            if (!canBuild(event, block, player, blockData)) return;
            // Axes: X → Y → Z
            Orientable orientableData = (Orientable) blockData;
            Axis blockAxis = orientableData.getAxis();
            if (blockAxis.equals(Axis.X)) {
                orientableData.setAxis(Axis.Y);
            } else if (blockAxis.equals(Axis.Y)) {
                orientableData.setAxis(Axis.Z);
            } else if (blockAxis.equals(Axis.Z)) {
                orientableData.setAxis(Axis.X);
            }
            block.setBlockData(orientableData, false);
        } else if (isRotatable(block)) {
            if (!canBuild(event, block, player, blockData)) return;
            final Rotatable rotatableData = (Rotatable) blockData;
            final BlockFace blockRotation = rotatableData.getRotation();
            rotatableData.setRotation(ROTATE_CCW_MAP.get(blockRotation));
            block.setBlockData(rotatableData, false);
        } else if (block.getType().equals(Material.RAIL)) {
            if (!canBuild(event, block, player, blockData)) return;
            BlockFace playerFacing = snapFace(event.getPlayer().getFacing());
            if (!lastRailFace.containsKey(block)) {
                Rail.Shape railShape = ((Rail) blockData).getShape();
                switch (railShape) {
                    case SOUTH_WEST:
                    case NORTH_SOUTH:
                    case SOUTH_EAST:
                        lastRailFace.put(block, new LastRailState(BlockFace.SOUTH, System.currentTimeMillis()));
                        break;
                    case NORTH_WEST:
                        lastRailFace.put(block, new LastRailState(BlockFace.NORTH, System.currentTimeMillis()));
                        break;
                    case NORTH_EAST:
                    case EAST_WEST:
                        lastRailFace.put(block, new LastRailState(BlockFace.EAST, System.currentTimeMillis()));
                        break;
                }
            }
            if (lastRailFace.get(block).face.equals(BlockFace.NORTH)) {
                switch (playerFacing) {
                    case SOUTH:
                        ((Rail) blockData).setShape(Rail.Shape.NORTH_SOUTH);
                        break;
                    case WEST:
                        ((Rail) blockData).setShape(Rail.Shape.NORTH_WEST);
                        break;
                    case EAST:
                        ((Rail) blockData).setShape(Rail.Shape.NORTH_EAST);
                        break;
                }
            } else if (lastRailFace.get(block).face.equals(BlockFace.EAST)) {
                switch (playerFacing) {
                    case SOUTH:
                        ((Rail) blockData).setShape(Rail.Shape.SOUTH_EAST);
                        break;
                    case WEST:
                        ((Rail) blockData).setShape(Rail.Shape.EAST_WEST);
                        break;
                    case NORTH:
                        ((Rail) blockData).setShape(Rail.Shape.NORTH_EAST);
                        break;
                }
            } else if (lastRailFace.get(block).face.equals(BlockFace.SOUTH)) {
                switch (playerFacing) {
                    case EAST:
                        ((Rail) blockData).setShape(Rail.Shape.SOUTH_EAST);
                        break;
                    case WEST:
                        ((Rail) blockData).setShape(Rail.Shape.SOUTH_WEST);
                        break;
                    case NORTH:
                        ((Rail) blockData).setShape(Rail.Shape.NORTH_SOUTH);
                        break;
                }
            } else if (lastRailFace.get(block).face.equals(BlockFace.WEST)) {
                switch (playerFacing) {
                    case SOUTH:
                        ((Rail) blockData).setShape(Rail.Shape.SOUTH_WEST);
                        break;
                    case EAST:
                        ((Rail) blockData).setShape(Rail.Shape.EAST_WEST);
                        break;
                    case NORTH:
                        ((Rail) blockData).setShape(Rail.Shape.NORTH_WEST);
                        break;
                }
            }
            lastRailFace.put(block, new LastRailState(playerFacing, System.currentTimeMillis()));
            block.setBlockData(blockData);
        }
    }


    @EventHandler
    public void onWrenchInteractEntity(PlayerInteractEntityEvent event) {
        ItemStack item = Objects.requireNonNull(event.getPlayer().getEquipment()).getItem(event.getHand());
        final @Nullable ItemMeta meta = Objects.requireNonNull(item).getItemMeta();
        if (meta == null) return;
        if (!Objects.equals(meta.getPersistentDataContainer().get(WrenchKey, PersistentDataType.BYTE), (byte) 1))
            return;

        final Player player = event.getPlayer();
        if (!player.hasPermission("polywrench.use")) return;
        if (player.isSneaking()) return;

        event.setCancelled(true);

        Entity entity = event.getRightClicked();

        if (entity instanceof ItemFrame) {
            if (((ItemFrame) entity).getItem().getType().equals(Material.AIR)) return;
            ((ItemFrame) entity).setVisible(!((ItemFrame) entity).isVisible());
        }
    }

    @EventHandler
    public void onInvisibleItemFrameItemPop(EntityDamageByEntityEvent event) {
        Entity entity = event.getEntity();
        if (entity instanceof ItemFrame && !((ItemFrame) entity).isVisible()) {
            entity.getWorld().dropItemNaturally(entity.getLocation(), new ItemStack(Material.ITEM_FRAME));
            getServer().getScheduler().scheduleSyncDelayedTask(this, entity::remove);
        }
    }

    private static BlockFace getPointedToDirection(final @NotNull Player player) {
        RayTraceResult rt = player.rayTraceBlocks(5.0);
        assert rt != null;
        final Block block = rt.getHitBlock();
        final Vector hitPos = rt.getHitPosition().subtract(
                new Vector(block.getX(), block.getY(), block.getZ()));
        final boolean northWest = hitPos.getX() < (1 - hitPos.getZ());
        final boolean northEast = hitPos.getX() > hitPos.getZ();
        if (northEast && northWest) {
            return BlockFace.NORTH;
        } else if (!northEast && northWest) {
            return BlockFace.WEST;
        } else if (!northEast && !northWest) {
            return BlockFace.SOUTH;
        } else {
            return BlockFace.EAST;
        }
    }

    private boolean canBuild(PlayerInteractEvent oEvent, Block block, Player player, BlockData blockData) {
        // Ideal would be using BlockCanBuildEvent but GriefPrevention doesn't support that
        final BlockPlaceEvent event = new BlockPlaceEvent(
                block, block.getState(), block, player.getInventory().getItem(oEvent.getHand()), player, true, oEvent.getHand());
        getServer().getPluginManager().callEvent(event);
        return event.canBuild() && !event.isCancelled();
    }

    /**
     * Returns whether the block is directional or not
     * @param block A block
     * @return true or false
     */
    private boolean isDirectional(Block block) {
        final BlockData blockData = block.getBlockData();
        if (!(blockData instanceof Directional)) {
            return false;
        }
        final Material blockType = block.getType();
        return !(BANNED_DIRECTIONAL.contains(blockType)
                || (blockData instanceof FaceAttachable && ((FaceAttachable) blockData).getAttachedFace() == FaceAttachable.AttachedFace.WALL)
                || blockData instanceof Bed || blockData instanceof Cocoa
                || blockData instanceof CommandBlock || blockData instanceof CoralWallFan || blockData instanceof EndPortalFrame
                || blockData instanceof Ladder || blockData instanceof RedstoneWallTorch || blockData instanceof TechnicalPiston
                || blockData instanceof TripwireHook || blockData instanceof WallSign
                || (blockData instanceof Piston && ((Piston) blockData).isExtended())
                || (blockData instanceof Bell && (((Bell) blockData).getAttachment() == Bell.Attachment.SINGLE_WALL || ((Bell) blockData).getAttachment() == Bell.Attachment.DOUBLE_WALL)));
    }

    /**
     * Returns whether the block is multiple facing or not
     * @param block A block
     * @return true or false
     */
    private boolean isMultipleFacing(Block block) {
        final BlockData blockData = block.getBlockData();
        return blockData instanceof Fence || blockData instanceof GlassPane;
    }

    /**
     * Returns whether the block is orientable or not
     * @param block A block
     * @return true or false
     */
    private boolean isOrientable(Block block) {
        BlockData blockData = block.getBlockData();
        Material blockType = block.getType();
        return blockData instanceof Orientable && !(blockType.equals(Material.NETHER_PORTAL));
    }

    /**
     * Returns whether the block is rotatable or not
     * @param block A block
     * @return true or false
     */
    private boolean isRotatable(Block block) {
        BlockData blockData = block.getBlockData();
        return blockData instanceof Rotatable;
    }

    /**
     * Return closest cardinal point
     * @param face The face
     * @return closest face
     */
    private BlockFace snapFace(BlockFace face) {
        BlockFace snappedFace;
        switch (face) {
            case EAST_NORTH_EAST:
            case EAST_SOUTH_EAST:
                snappedFace = BlockFace.EAST;
                break;
            case NORTH_NORTH_EAST:
            case NORTH_NORTH_WEST:
            case NORTH_EAST:
            case NORTH_WEST:
                snappedFace = BlockFace.NORTH;
                break;
            case SOUTH_SOUTH_EAST:
            case SOUTH_SOUTH_WEST:
            case SOUTH_EAST:
            case SOUTH_WEST:
                snappedFace = BlockFace.SOUTH;
                break;
            case WEST_NORTH_WEST:
            case WEST_SOUTH_WEST:
                snappedFace = BlockFace.WEST;
                break;
            default:
                snappedFace = face;
        }
        return snappedFace;
    }
}
