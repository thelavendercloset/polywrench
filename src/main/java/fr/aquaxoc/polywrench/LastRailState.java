package fr.aquaxoc.polywrench;

import org.bukkit.block.BlockFace;

public class LastRailState {
    public BlockFace face;
    public long dateUsed;

    public LastRailState (BlockFace face, long dateUsed) {
        this.face = face;
        this.dateUsed = dateUsed;
    }
}
